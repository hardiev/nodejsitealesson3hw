const express = require('express');
const app = express();
const port = 8000;
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');

app.use('/', express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use((req, res, next) => {
  fs.readFile(path.join(__dirname, 'db.json'), (error, data) => {
    if (error) {
      console.log('error');
    }
    const db = data;
    res.locals.db = JSON.parse(db.toString());
    next();
  })
});

app.route('/orders')
  .get((req, res) => {
    res.status(200).json(res.locals.db)
  })
  .post((req, res) => {
    let body = {order: req.body};
    let newId = Number(Object.keys(res.locals.db).reverse()[0]) + 1;
    res.locals.db[newId] = body;
    fs.writeFile(path.join(__dirname, 'db.json'), JSON.stringify(res.locals.db), (err) => {
      if (err) throw err;
      res.send(`order "${body.order}" with id ${newId} was created`).end();
    })
  })

app.route('/orders/:id')
  .get((req, res) => {
    if (Object.keys(res.locals.db).indexOf(req.params.id) >= 0) {
      res.status(200).json(res.locals.db[req.params.id])
    } else {
      res.status(200).json({
        order: "There is no such item"
      })
    }
  })
  .put((req, res) => {
    let idOrder = req.params.id;
    console.log(idOrder);
    let body = {order: req.body};
    if (Object.keys(res.locals.db).indexOf(req.params.id) >= 0) {
      res.locals.db[idOrder] = body;
      fs.writeFile(path.join(__dirname, 'db.json'), JSON.stringify(res.locals.db), (err) => {
        if (err) throw err;
        res.send(`order with id ${idOrder} was updated. New order is ${body.order}`).end();
      })
    } else {
      res.send(`There is no such item`).end();
    }
  })
  .delete((req, res) => {
    if (Object.keys(res.locals.db).indexOf(req.params.id) >= 0) {
      delete res.locals.db[req.params.id];
      fs.writeFile(path.join(__dirname, 'db.json'), JSON.stringify(res.locals.db), (err) => {
        if (err) throw err;
        res.send(`order with id ${req.params.id} was deleted`).end();
      })
    } else {
      res.send(`There is no such item`).end();
    }
  })

app.listen(port, () => console.log(`Listen on port ${port}`));